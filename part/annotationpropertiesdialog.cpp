/*
    SPDX-FileCopyrightText: 2006 Chu Xiaodong <xiaodongchu@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "annotationpropertiesdialog.h"

// qt/kde includes
#include <KLineEdit>
#include <KLocalizedString>
#include <QFormLayout>
#include <QFrame>
#include <QHeaderView>
#include <QIcon>
#include <QLabel>
#include <QLayout>
#include <QPushButton>
#include <QTextEdit>

// local includes
#include "annotationwidgets.h"
#include "core/annotations.h"
#include "core/document.h"
#include "core/page.h"

AnnotsPropertiesDialog::AnnotsPropertiesDialog(QWidget *parent, Okular::Document *document, const QList<Okular::AnnotPagePair> &anns)
    : KPageDialog(parent)
    , m_document(document)
    , modified(false)
    , m_annots(anns)
{
    Q_ASSERT(m_annots.size());
    if (m_annots.size() == 0) {
        return;
    }

    setFaceType(Tabbed);
    const bool canEditAnnotations = std::all_of(m_annots.cbegin(), m_annots.cend(), [this](const auto &ann){ return m_document->canModifyPageAnnotation(ann.page, ann.annotation); });
    setCaptionTextbyAnnotType();
    if (canEditAnnotations) {
        setStandardButtons(QDialogButtonBox::Ok | QDialogButtonBox::Apply | QDialogButtonBox::Cancel);
        button(QDialogButtonBox::Apply)->setEnabled(false);
        connect(button(QDialogButtonBox::Apply), &QPushButton::clicked, this, &AnnotsPropertiesDialog::slotapply);
        connect(button(QDialogButtonBox::Ok), &QPushButton::clicked, this, &AnnotsPropertiesDialog::slotapply);
    } else {
        setStandardButtons(QDialogButtonBox::Close);
        button(QDialogButtonBox::Close)->setDefault(true);
    }

    QLabel *tmplabel;

    // Appearance tabs
    m_appearanceWidgets = AnnotationWidgetFactory::appearanceWidgetsFor(this, m_annots);
    for (auto &widg : m_appearanceWidgets) {
        widg->setEnabled(canEditAnnotations);
        addPage(widg, widg->windowTitle());
    }

    // General tab
    QFrame *page = new QFrame(this);
    addPage(page, i18n("&General"));
    //    m_tabitem[1]->setIcon( QIcon::fromTheme( "fonts" ) );
    QFormLayout *gridlayout = new QFormLayout(page);
    
    bool authorsMatch = true, createdMatch = true, modifiedMatch = true;
    for (int i = 1; i < m_annots.size(); i++) {
        authorsMatch &= m_annots[0].annotation->author() == m_annots[i].annotation->author();
        createdMatch &= m_annots[0].annotation->creationDate() == m_annots[i].annotation->creationDate();
        modifiedMatch &= m_annots[0].annotation->modificationDate() == m_annots[i].annotation->modificationDate();
        if (!authorsMatch && !createdMatch && !modifiedMatch) {
            break;
        }
    }
    if (authorsMatch) {
        AuthorEdit = new KLineEdit(m_annots[0].annotation->author(), page);
    } else {
        AuthorEdit = new KLineEdit(page);
        AuthorEdit->setPlaceholderText(i18n("varies"));
    }
    AuthorEdit->setEnabled(canEditAnnotations);
    gridlayout->addRow(i18n("&Author:"), AuthorEdit);

    if (createdMatch) {
        tmplabel = new QLabel(QLocale().toString(m_annots[0].annotation->creationDate(), QLocale::LongFormat), page);
    } else {
        tmplabel = new QLabel(i18n("varies"), page);
    }
    tmplabel->setTextInteractionFlags(Qt::TextSelectableByMouse);
    gridlayout->addRow(i18n("Created:"), tmplabel);

    if (modifiedMatch) {
        m_modifyDateLabel = new QLabel(QLocale().toString(m_annots[0].annotation->modificationDate(), QLocale::LongFormat), page);
    } else {
        m_modifyDateLabel = new QLabel(i18n("varies"), page);
    }
    m_modifyDateLabel->setTextInteractionFlags(Qt::TextSelectableByMouse);
    gridlayout->addRow(i18n("Modified:"), m_modifyDateLabel);

    // Extra tabs 
    m_appearanceWidgets = AnnotationWidgetFactory::extraWidgetsFor(this, m_annots);
    for (auto &widg : m_appearanceWidgets) {
        addPage(widg, widg->windowTitle());
    }

    // BEGIN connections
    connect(AuthorEdit, &QLineEdit::textChanged, this, &AnnotsPropertiesDialog::setModified);
    for (auto &widg : m_appearanceWidgets) {
        connect(widg, &AnnotationWidget::dataChanged, this, &AnnotsPropertiesDialog::setModified);
    }
    for (auto &widg : m_extraWidgets) {
        connect(widg, &AnnotationWidget::dataChanged, this, &AnnotsPropertiesDialog::setModified);
    }
    // END

#if 0
    qCDebug(OkularUiDebug) << "Annotation details:";
    qCDebug(OkularUiDebug).nospace() << " => unique name: '" << ann->uniqueName() << "'";
    qCDebug(OkularUiDebug) << " => flags:" << QString::number( m_annot->flags(), 2 );
#endif

    resize(sizeHint());
}
AnnotsPropertiesDialog::~AnnotsPropertiesDialog()
{
}

void AnnotsPropertiesDialog::setCaptionTextbyAnnotType()
{
    Okular::Annotation::SubType type = m_annot->subType();
    QString captiontext;
    switch (type) {
    case Okular::Annotation::AText:
        if (((Okular::TextAnnotation *)m_annot)->textType() == Okular::TextAnnotation::Linked) {
            captiontext = i18n("Pop-up Note Properties");
        } else {
            if (((Okular::TextAnnotation *)m_annot)->inplaceIntent() == Okular::TextAnnotation::TypeWriter) {
                captiontext = i18n("Typewriter Properties");
            } else {
                captiontext = i18n("Inline Note Properties");
            }
        }
        break;
    case Okular::Annotation::ALine:
        if (((Okular::LineAnnotation *)m_annot)->linePoints().count() == 2) {
            captiontext = i18n("Straight Line Properties");
        } else {
            captiontext = i18n("Polygon Properties");
        }
        break;
    case Okular::Annotation::AGeom:
        captiontext = i18n("Geometry Properties");
        break;
    case Okular::Annotation::AHighlight:
        captiontext = i18n("Text Markup Properties");
        break;
    case Okular::Annotation::AStamp:
        captiontext = i18n("Stamp Properties");
        break;
    case Okular::Annotation::AInk:
        captiontext = i18n("Freehand Line Properties");
        break;
    case Okular::Annotation::ACaret:
        captiontext = i18n("Caret Properties");
        break;
    case Okular::Annotation::AFileAttachment:
        captiontext = i18n("File Attachment Properties");
        break;
    case Okular::Annotation::ASound:
        captiontext = i18n("Sound Properties");
        break;
    case Okular::Annotation::AMovie:
        captiontext = i18n("Movie Properties");
        break;
    default:
        captiontext = i18n("Annotation Properties");
        break;
    }
    setWindowTitle(captiontext);
}

void AnnotsPropertiesDialog::setModified()
{
    modified = true;
    button(QDialogButtonBox::Apply)->setEnabled(true);
}

void AnnotsPropertiesDialog::slotapply()
{
    if (!modified) {
        return;
    }

    m_document->prepareToModifyAnnotationProperties(m_annot);
    m_annot->setAuthor(AuthorEdit->text());
    m_annot->setModificationDate(QDateTime::currentDateTime());

    for (auto &widg : m_appearanceWidgets) {
        widg->applyChanges();
    }
    for (auto &widg : m_extraWidgets) {
        widg->applyChanges();
    }

    for (auto &annot : m_annots) {
        m_document->modifyPageAnnotationProperties(m_page, annot);

        m_modifyDateLabel->setText(i18n("Modified: %1", QLocale().toString(annot->modificationDate(), QLocale::LongFormat)));
    }

    modified = false;
    button(QDialogButtonBox::Apply)->setEnabled(false);
}

#include "moc_annotationpropertiesdialog.cpp"
