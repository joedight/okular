/*
    SPDX-FileCopyrightText: 2006 Tobias Koenig <tokoe@kde.org>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#ifndef ANNOTATIONPOPUP_H
#define ANNOTATIONPOPUP_H

#include <QList>
#include <QObject>
#include <QPair>
#include <QPoint>

class QMenu;

namespace Okular
{
class Annotation;
class Document;
class AnnotPagePair;
}

class AnnotationPopup : public QObject
{
    Q_OBJECT

public:
    /**
     * Describes the structure of the popup menu.
     */
    enum MenuMode {
        SingleAnnotationMode, ///< The menu shows only entries to manipulate a single annotation, or multiple annotations as a group.
        MultiAnnotationMode   ///< The menu shows entries to manipulate multiple annotations.
    };

    AnnotationPopup(Okular::Document *document, MenuMode mode, QWidget *parent = nullptr);

    void addAnnotation(Okular::Annotation *annotation, int pageNumber);

    /* You only need to use this if you don't plan on using exec() */
    void addActionsToMenu(QMenu *menu);

    void exec(const QPoint point = QPoint());

Q_SIGNALS:
    void openAnnotationWindow(Okular::Annotation *annotation, int pageNumber);

    private:
    void doCopyAnnotation(const Okular::AnnotPagePair &pair);
    void doRemovePageAnnotation(const Okular::AnnotPagePair &pair);
    void doOpenAnnotationWindow(const Okular::AnnotPagePair &pair);
    void doOpenPropertiesDialog(const QList<Okular::AnnotPagePair> &pair);
    void doSaveEmbeddedFile(const Okular::AnnotPagePair &pair);

    QWidget *mParent;

    QList<Okular::AnnotPagePair> mAnnotations;
    Okular::Document *mDocument;
    MenuMode mMenuMode;
};

#endif
