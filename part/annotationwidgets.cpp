/*
    SPDX-FileCopyrightText: 2006 Pino Toscano <pino@kde.org>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "annotationwidgets.h"

// qt/kde includes
#include <KColorButton>
#include <KComboBox>
#include <KFontRequester>
#include <KFormat>
#include <KLocalizedString>
#include <KMessageBox>
#include <KMessageWidget>
#include <QCheckBox>
#include <QDebug>
#include <QFileDialog>
#include <QFormLayout>
#include <QGroupBox>
#include <QGuiApplication>
#include <QIcon>
#include <QLabel>
#include <QLayout>
#include <QList>
#include <QMimeDatabase>
#include <QPair>
#include <QSize>
#include <QSpinBox>
#include <QVariant>

#include <algorithm>

#include "core/annotations.h"
#include "core/annotations_p.h"
#include "core/document.h"
#include "core/document_p.h"
#include "core/page_p.h"
#include "gui/guiutils.h"
#include "gui/pagepainter.h"

#define FILEATTACH_ICONSIZE 48

PixmapPreviewSelector::PixmapPreviewSelector(QWidget *parent, PreviewPosition position)
    : QWidget(parent)
    , m_previewPosition(position)
{
    QVBoxLayout *mainlay = new QVBoxLayout(this);
    mainlay->setContentsMargins(0, 0, 0, 0);
    QHBoxLayout *toplay = new QHBoxLayout;
    toplay->setContentsMargins(0, 0, 0, 0);
    mainlay->addLayout(toplay);
    m_comboItems = new KComboBox(this);
    toplay->addWidget(m_comboItems);
    m_stampPushButton = new QPushButton(QIcon::fromTheme(QStringLiteral("document-open")), QString(), this);
    m_stampPushButton->setVisible(false);
    m_stampPushButton->setToolTip(i18nc("@info:tooltip", "Select a custom stamp symbol from file"));
    toplay->addWidget(m_stampPushButton);
    m_iconLabel = new QLabel(this);
    switch (m_previewPosition) {
    case Side:
        toplay->addWidget(m_iconLabel);
        break;
    case Below:
        mainlay->addWidget(m_iconLabel);
        mainlay->setAlignment(m_iconLabel, Qt::AlignHCenter);
        break;
    }
    m_iconLabel->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    m_iconLabel->setAlignment(Qt::AlignCenter);
    m_iconLabel->setFrameStyle(QFrame::StyledPanel);
    setPreviewSize(32);

    setFocusPolicy(Qt::TabFocus);
    setFocusProxy(m_comboItems);

    connect(m_comboItems, &QComboBox::currentIndexChanged, this, [this](int index) { iconComboChanged(m_comboItems->itemText(index)); });
    connect(m_comboItems, &QComboBox::editTextChanged, this, &PixmapPreviewSelector::iconComboChanged);
    connect(m_stampPushButton, &QPushButton::clicked, this, &PixmapPreviewSelector::selectCustomStamp);
}

PixmapPreviewSelector::~PixmapPreviewSelector()
{
}

void PixmapPreviewSelector::setIcon(const QString &icon)
{
    int id = m_comboItems->findData(QVariant(icon), Qt::UserRole, Qt::MatchFixedString);
    if (id == -1) {
        id = m_comboItems->findText(icon, Qt::MatchFixedString);
    }
    if (id > -1) {
        m_comboItems->setCurrentIndex(id);
    } else if (m_comboItems->isEditable()) {
        m_comboItems->addItem(icon, QVariant(icon));
        m_comboItems->setCurrentIndex(m_comboItems->findText(icon, Qt::MatchFixedString));
    }
}

QString PixmapPreviewSelector::icon() const
{
    return m_icon;
}

void PixmapPreviewSelector::addItem(const QString &item, const QString &id)
{
    m_comboItems->addItem(item, QVariant(id));
    setIcon(m_icon);
}

void PixmapPreviewSelector::setPreviewSize(int size)
{
    m_previewSize = size;
    switch (m_previewPosition) {
    case Side:
        m_iconLabel->setFixedSize(m_previewSize + 8, m_previewSize + 8);
        break;
    case Below:
        m_iconLabel->setFixedSize(3 * m_previewSize + 8, m_previewSize + 8);
        break;
    }
    iconComboChanged(m_icon);
}

int PixmapPreviewSelector::previewSize() const
{
    return m_previewSize;
}

void PixmapPreviewSelector::setEditable(bool editable)
{
    m_comboItems->setEditable(editable);
    m_stampPushButton->setVisible(editable);
}

void PixmapPreviewSelector::iconComboChanged(const QString &icon)
{
    int id = m_comboItems->findText(icon, Qt::MatchFixedString);
    if (id >= 0) {
        m_icon = m_comboItems->itemData(id).toString();
    } else {
        m_icon = icon;
    }

    QPixmap pixmap = Okular::AnnotationUtils::loadStamp(m_icon, m_previewSize);
    const QRect cr = m_iconLabel->contentsRect();
    if (pixmap.width() > cr.width() || pixmap.height() > cr.height()) {
        pixmap = pixmap.scaled(cr.size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
    }
    m_iconLabel->setPixmap(pixmap);

    Q_EMIT iconChanged(m_icon);
}

void PixmapPreviewSelector::selectCustomStamp()
{
    const QString customStampFile = QFileDialog::getOpenFileName(this, i18nc("@title:window file chooser", "Select custom stamp symbol"), QString(), i18n("*.ico *.png *.xpm *.svg *.svgz | Icon Files (*.ico *.png *.xpm *.svg *.svgz)"));
    if (!customStampFile.isEmpty()) {
        QPixmap pixmap = Okular::AnnotationUtils::loadStamp(customStampFile, m_previewSize);
        if (pixmap.isNull()) {
            KMessageBox::error(this, xi18nc("@info", "Could not load the file <filename>%1</filename>", customStampFile), i18nc("@title:window", "Invalid file"));
        } else {
            m_comboItems->setEditText(customStampFile);
        }
    }
}

AnnotationWidget *AnnotationWidgetFactory::appearanceWidgetFor(QWidget *parent, Okular::Annotation *ann)
{
    QList<AnnotationWidget*> ws = AnnotationWidgetFactory::appearanceWidgetsFor(parent, {ann});
    Q_ASSERT(ws.size() == 1);
    return ws.size() ? ws[0] : nullptr;
}

QList<AnnotationWidget*> AnnotationWidgetFactory::appearanceWidgetsFor(QWidget *parent, const QList<Okular::Annotation*> &anns)
{
    Q_ASSERT(anns.size());
    if (!anns.size()) {
        return {};
    }

    // Further divide where configuration significantly differs inside a subtype
    enum class SubSubtype {
        General = 0,
        TwopointLine,
        PolygonLine,
    };
    QMultiMap<QPair<Okular::Annotation::SubType, SubSubtype>, Okular::Annotation*> bytype;
    for (auto *ann : anns) {
        auto subsubtype = SubSubtype::General;
        if (ann->subType() == Okular::Annotation::ALine) {
            auto lineAnn = static_cast<Okular::LineAnnotation *>(ann);
            if (lineAnn->linePoints().count() == 2) {
                subsubtype = SubSubtype::TwopointLine;
            } else if (lineAnn->lineClosed()) {
                subsubtype = SubSubtype::PolygonLine;
            }
        }
        bytype.insert(qMakePair(ann->subType(), subsubtype), ann);
    }
    bool singlePage = bytype.keys().size() == 1;

    QList<AnnotationWidget*> widgs;
    for (auto &typePair : bytype.keys()) {
            const auto type = typePair.first;
            const auto subsubtype = typePair.second;
            const AnnotationWidget::Flags flags = singlePage ? AnnotationWidget::IncludeSharedOptions : AnnotationWidget::ExcludeSharedOptions;
            switch (type) {
	    case Okular::Annotation::AStamp:
                //widgs.append(new StampAnnotationWidget(parent, bytype[typePair], flags));
		break;
	    case Okular::Annotation::AText:
		widgs.append(new TextAnnotationWidget(parent, bytype.values(typePair), flags));
		break;
	    case Okular::Annotation::ALine:
                if (subsubtype == SubSubtype::TwopointLine) {
                    //widgs.append(new TwopointLineAnnotationWidget(parent, bytype[typePair], flags));
                } else if (subsubtype == SubSubtype::PolygonLine) {
                    //widgs.append(new PolygonLineAnnotationWidget(parent, bytype[typePair], flags));
                } else {
                    //widgs.append(new GeneralLineAnnotationWidget(parent, bytype[typePair], flags));
                }
		break;
	    case Okular::Annotation::AHighlight:
		//widgs.append(new HighlightAnnotationWidget(parent, bytype[typePair], flags));
		break;
	    case Okular::Annotation::AInk:
		//widgs.append(new InkAnnotationWidget(parent, bytype[typePair], flags));
		break;
	    case Okular::Annotation::AGeom:
		//widgs.append(new GeomAnnotationWidget(parent, bytype[typePair], flags));
		break;
	    case Okular::Annotation::AFileAttachment:
		//widgs.append(new FileAttachmentAnnotationWidget(parent, bytype[typePair], flags));
		break;
	    case Okular::Annotation::ACaret:
		//widgs.append(new CaretAnnotationWidget(parent, bytype[typePair], flags));
		break;
	    default:
                ;
	    }
    }

    if (widgs.size() == 0 || !singlePage) {
        widgs.push_front(new AnnotationWidget(parent, anns, AnnotationWidget::IncludeSharedOptions));
    }

    return widgs;
}
AnnotationWidget *AnnotationWidgetFactory::extraWidgetFor(QWidget *parent, Okular::Annotation *ann)
{
    return nullptr;
}

AnnotationWidget::AnnotationWidget(QWidget *parent, const QList<Okular::Annotation*> &anns, AnnotationWidget::Flags flags)
    : QWidget(parent)
    , m_typeEditable(true)
    , m_anns(anns)
{
    m_formlayout = new QFormLayout(this);

    m_formlayout->setLabelAlignment(Qt::AlignRight);
    m_formlayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);

    if (flags & IncludeSharedOptions) {
        addColorPicker();
        addOpacitySpinBox();
    }
}

void AnnotationWidget::applyChanges()
{
    for (auto *ann : m_anns) {
        if (m_colorBn) {
            ann->style().setColor(m_colorBn->color());
        }
        if (m_opacity) {
            ann->style().setOpacity((double)m_opacity->value() / 100.0);
        }
    }
}

QSet<Okular::Annotation*> AnnotationWidget::dirtyAnnotations()
{
    QSet<Okular::Annotation*> ret;
    for (auto *ann : m_anns) {
        if ((m_colorBn && ann->style().color() != m_colorBn->color()) ||
            (m_opacity && int(ann->style().opacity() * 100.0) != m_opacity->value())
        ) {
            ret.insert(ann);
        }
    }
    return ret;
}

void AnnotationWidget::addColorPicker()
{
    if (m_anns.size() && std::all_of(m_anns.cbegin(), m_anns.cend(),
        [this](auto x){ return x->style().color() == m_anns[0]->style().color(); })
    ) {
        m_colorBn = new KColorButton(m_anns[0]->style().color(), this);
    } else {
        m_colorBn = new KColorButton(this);
    }
    m_formlayout->addRow(i18n("&Color:"), m_colorBn);
    connect(m_colorBn, &KColorButton::changed, this, &AnnotationWidget::dataChanged);
}

void AnnotationWidget::addOpacitySpinBox()
{
    m_opacity = new QSpinBox(this);
    m_opacity->setRange(0, 100);

    if (m_anns.size() && std::all_of(m_anns.cbegin(), m_anns.cend(),
        [this](auto x){ return x->style().opacity() == m_anns[0]->style().opacity(); })
    ) {
        m_opacity->setValue((int)(m_ann->style().opacity() * 100.0));
    } else {
        m_opacity->setSpecialText(i18nc("Annotations have differing opacities, which remain unchanged", "varies"));
        m_opacity->setValue(-1);
    }
    // FIXME
    m_opacity->setSuffix(i18nc("Suffix for the opacity level, eg '80%'", "%"));
    m_formlayout->addRow(i18n("&Opacity:"), m_opacity);
    connect(m_opacity, QOverload<int>::of(&QSpinBox::valueChanged), this, &AnnotationWidget::dataChanged);
}

void AnnotationWidget::addVerticalSpacer()
{
    if (m_formlayout->rowCount()) {
        m_formlayout->addItem(new QSpacerItem(0, 5, QSizePolicy::Fixed, QSizePolicy::Fixed));
    }
}

void AnnotationWidget::setAnnotTypeEditable(bool editable)
{
    m_typeEditable = editable;
}

TextAnnotationWidget::TextAnnotationWidget(QWidget *parent, const QList<Okular::Annotation*> &anns, AnnotationWidget::Flags flags)
    : AnnotationWidget(parent, anns, AnnotationWidget::ExcludeSharedOptions)
{
    for (auto *ann : anns) {
        auto textAnn = static_cast<Okular::TextAnnotation *>(ann);
        if (textAnn->textType() == Okular::TextAnnotation::Linked) {
            m_textLinked.append(textAnn);
        } else if (textAnn->textType() == Okular::TextAnnotation::InPlace) {
            if (textAnn->inplaceIntent() == Okular::TextAnnotation::TypeWriter) {
                m_textTypewriter.append(textAnn);
            } else {
                m_textInplace.append(textAnn);
            }
        }
    }


    bool follows = false;
    auto maybeSpace = [this, &follows](){
        if (follows) { 
            addVerticalSpacer();
        }
        follows = true;
    };
    if (m_textLinked.size() || m_textInplace.size()) {
        maybeSpace();
        addColorPicker();
        addOpacitySpinBox();
    }
    if (m_textLinked.size()) {
        maybeSpace();
        addPixmapSelector();
    }
    if (m_textTypewriter.size() || m_textInplace.size()) {
        maybeSpace();
        addFontRequester();
    }
    if (m_textTypewriter.size()) {
        addTextColorButton();
    }
    if (m_textInplace.size()) {
        addTextAlignComboBox();
        addVerticalSpacer();
        addWidthSpinBox();
    }
}

void TextAnnotationWidget::applyChanges()
{
    AnnotationWidget::applyChanges();
    for (auto *txt : m_textLinked) {
        Q_ASSERT(m_pixmapSelector);
        txt->setTextIcon(m_pixmapSelector->icon());
        txt->style().setColor(m_colorBn->color());
        txt->style().setOpacity((double)m_opacity->value() / 100.0);
    }
    for (auto *txt : m_textTypewriter) {
        Q_ASSERT(m_fontReq && m_textColorBn);
        txt->setTextFont(m_fontReq->font());
        txt->setTextColor(m_textColorBn->color());
    }
    for (auto *txt : m_textInplace) {
        Q_ASSERT(m_fontReq && m_textAlign && m_spinWidth);
        txt->setTextFont(m_fontReq->font());
        txt->setInplaceAlignment(m_textAlign->currentIndex());
        txt->style().setWidth(m_spinWidth->value());
        txt->style().setColor(m_colorBn->color());
        txt->style().setOpacity((double)m_opacity->value() / 100.0);
    }
}

void TextAnnotationWidget::addPixmapSelector()
{
    m_pixmapSelector = new PixmapPreviewSelector(this);
    m_formlayout->addRow(i18n("Icon:"), m_pixmapSelector);
    m_pixmapSelector->addItem(i18n("Comment"), QStringLiteral("Comment"));
    m_pixmapSelector->addItem(i18n("Help"), QStringLiteral("Help"));
    m_pixmapSelector->addItem(i18n("Insert"), QStringLiteral("Insert"));
    m_pixmapSelector->addItem(i18n("Key"), QStringLiteral("Key"));
    m_pixmapSelector->addItem(i18n("New paragraph"), QStringLiteral("NewParagraph"));
    m_pixmapSelector->addItem(i18n("Note"), QStringLiteral("Note"));
    m_pixmapSelector->addItem(i18n("Paragraph"), QStringLiteral("Paragraph"));
    // FIXME
    //m_pixmapSelector->setIcon(m_textAnn->textIcon());
    connect(m_pixmapSelector, &PixmapPreviewSelector::iconChanged, this, &AnnotationWidget::dataChanged);
}

void TextAnnotationWidget::addFontRequester()
{
    m_fontReq = new KFontRequester(this);
    m_formlayout->addRow(i18n("Font:"), m_fontReq);
    //FIXME
    //m_fontReq->setFont(m_textAnn->textFont());
    connect(m_fontReq, &KFontRequester::fontSelected, this, &AnnotationWidget::dataChanged);
}

void TextAnnotationWidget::addTextColorButton()
{
    m_textColorBn = new KColorButton(this);
    // FIXME
    //m_textColorBn->setColor(m_textAnn->textColor());
    m_formlayout->addRow(i18n("Text &color:"), m_textColorBn);
    connect(m_textColorBn, &KColorButton::changed, this, &AnnotationWidget::dataChanged);
}

void TextAnnotationWidget::addTextAlignComboBox()
{
    m_textAlign = new KComboBox(this);
    m_formlayout->addRow(i18n("&Align:"), m_textAlign);
    m_textAlign->addItem(i18n("Left"));
    m_textAlign->addItem(i18n("Center"));
    m_textAlign->addItem(i18n("Right"));
    // FIXME
    //m_textAlign->setCurrentIndex(m_textAnn->inplaceAlignment());
    connect(m_textAlign, QOverload<int>::of(&KComboBox::currentIndexChanged), this, &AnnotationWidget::dataChanged);
}

void TextAnnotationWidget::addWidthSpinBox()
{
    m_spinWidth = new QDoubleSpinBox(this);
    m_formlayout->addRow(i18n("Border &width:"), m_spinWidth);
    m_spinWidth->setRange(0, 100);
    // FIXME
    //m_spinWidth->setValue(m_textAnn->style().width());
    m_spinWidth->setSingleStep(0.1);
    connect(m_spinWidth, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &AnnotationWidget::dataChanged);
}

const QList<QPair<QString, QString>> &StampAnnotationWidget::defaultStamps()
{
    static const QList<QPair<QString, QString>> defaultStampsList = {
        {i18n("Approved"), QStringLiteral("Approved")},
        {i18n("As Is"), QStringLiteral("AsIs")},
        {i18n("Confidential"), QStringLiteral("Confidential")},
        {i18n("Departmental"), QStringLiteral("Departmental")},
        {i18n("Draft"), QStringLiteral("Draft")},
        {i18n("Experimental"), QStringLiteral("Experimental")},
        {i18n("Expired"), QStringLiteral("Expired")},
        {i18n("Final"), QStringLiteral("Final")},
        {i18n("For Comment"), QStringLiteral("ForComment")},
        {i18n("For Public Release"), QStringLiteral("ForPublicRelease")},
        {i18n("Not Approved"), QStringLiteral("NotApproved")},
        {i18n("Not For Public Release"), QStringLiteral("NotForPublicRelease")},
        {i18n("Sold"), QStringLiteral("Sold")},
        {i18n("Top Secret"), QStringLiteral("TopSecret")},
        {i18n("Bookmark"), QStringLiteral("bookmark-new")},
        {i18n("Information"), QStringLiteral("help-about")},
        {i18n("KDE"), QStringLiteral("kde")},
        {i18n("Okular"), QStringLiteral("okular")}
    };

    return defaultStampsList;
}

StampAnnotationWidget::StampAnnotationWidget(QWidget *parent, const QList<Okular::Annotation*> &anns, AnnotationWidget::Flags flags)
    : AnnotationWidget(parent, anns, AnnotationWidget::ExcludeSharedOptions)
{
    //auto stampAnn = static_cast<Okular::StampAnnotation *>(ann);

    addOpacitySpinBox();
    addVerticalSpacer();

    m_pixmapSelector = new PixmapPreviewSelector(this, PixmapPreviewSelector::Below);
    m_formlayout->addRow(i18n("Stamp symbol:"), m_pixmapSelector);
    m_pixmapSelector->setEditable(true);

    for (const QPair<QString, QString> &pair : defaultStamps()) {
        m_pixmapSelector->addItem(pair.first, pair.second);
    }

    // FIXME
    //m_pixmapSelector->setIcon(m_stampAnn->stampIconName());
    m_pixmapSelector->setPreviewSize(64);

    connect(m_pixmapSelector, &PixmapPreviewSelector::iconChanged, this, &AnnotationWidget::dataChanged);
}

void StampAnnotationWidget::applyChanges()
{
    AnnotationWidget::applyChanges();
    for (auto ann : m_anns) {
        auto stamp = static_cast<Okular::StampAnnotation *>(ann);
        stamp->setStampIconName(m_pixmapSelector->icon());
    }
}

/*

LineAnnotationWidget::LineAnnotationWidget(QObject *parent, const QList<Okular::Annotation*> &anns, AnnotationWidget::Flags flags)
    : AnnotationWidget(parent, anns, flags)
{
    addVerticalSpacer();

    addWidthSpinbox();
}

LineAnnotationWidget::addWidthSpinbox()
{
    Q_ASSERT(m_spinSize == nullptr);
    m_spinSize = new QDoubleSpinBox(this);
    m_spinSize->setRange(1, 100);
    m_formlayout->addRow(i18n("&Width:"), m_spinSize);
    // FIXME
    //m_spinSize->setValue(m_lineAnn->style().width());
    connect(m_spinSize, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &LineAnnotationWidget::dataChanged);
}

TwopointLineAnnotationWidget::TwopointLineAnnotationWidget(QObject *parent, const QList<Okular::Annotation*> &anns, AnnotationWidget::Flags flags)
    : AnnotationWidget(parent, anns, AnnotationWidget::StartEmpty)
{

    // Line Term Styles
    addVerticalSpacer();
    m_startStyleCombo = new QComboBox(this);
    formlayout->addRow(i18n("Line start:"), m_startStyleCombo);
    m_endStyleCombo = new QComboBox(this);
    formlayout->addRow(i18n("Line end:"), m_endStyleCombo);
    // FIXME: Where does the tooltip goes??

    const QList<QPair<Okular::LineAnnotation::TermStyle, QString>> termStyles {
        {Okular::LineAnnotation::Square, i18n("Square")},
        {Okular::LineAnnotation::Circle, i18n("Circle")},
        {Okular::LineAnnotation::Diamond, i18n("Diamond")},
        {Okular::LineAnnotation::OpenArrow, i18n("Open Arrow")},
        {Okular::LineAnnotation::ClosedArrow, i18n("Closed Arrow")},
        {Okular::LineAnnotation::None, i18n("None")},
        {Okular::LineAnnotation::Butt, i18n("Butt")},
        {Okular::LineAnnotation::ROpenArrow, i18n("Right Open Arrow")},
        {Okular::LineAnnotation::RClosedArrow, i18n("Right Closed Arrow")},
        {Okular::LineAnnotation::Slash, i18n("Slash")}
    };
    for (const auto &item : termStyles) {
        const QIcon icon = endStyleIcon(item.first, QGuiApplication::palette().color(QPalette::WindowText));
        m_startStyleCombo->addItem(icon, item.second);
        m_endStyleCombo->addItem(icon, item.second);
    }

    m_startStyleCombo->setCurrentIndex(m_lineAnn->lineStartStyle());
    m_endStyleCombo->setCurrentIndex(m_lineAnn->lineEndStyle());

    // Leaders lengths
    addVerticalSpacer();
    m_spinLL = new QDoubleSpinBox(this);
    formlayout->addRow(i18n("Leader line length:"), m_spinLL);
    m_spinLLE = new QDoubleSpinBox(this);
    formlayout->addRow(i18n("Leader line extensions length:"), m_spinLLE);

    m_spinLL->setRange(-500, 500);
    m_spinLL->setValue(m_lineAnn->lineLeadingForwardPoint());
    m_spinLLE->setRange(0, 500);
    m_spinLLE->setValue(m_lineAnn->lineLeadingBackwardPoint());

    connect(m_startStyleCombo, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &LineAnnotationWidget::dataChanged);
    connect(m_endStyleCombo, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &LineAnnotationWidget::dataChanged);
    connect(m_spinLL, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &LineAnnotationWidget::dataChanged);
    connect(m_spinLLE, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &LineAnnotationWidget::dataChanged);
}

        
PolygonLineAnnotationWidget::PolygonLineAnnotationWidget(QObject *parent, const QList<Okular::Annotation*> &anns, AnnotationWidget::Flags flags)
    : AnnotationWidget(parent, anns, AnnotationWidget::StartEmpty)
{
        QHBoxLayout *colorlay = new QHBoxLayout();
        m_useColor = new QCheckBox(i18n("Enabled"), this);
        colorlay->addWidget(m_useColor);
        m_innerColor = new KColorButton(this);
        colorlay->addWidget(m_innerColor);
        m_formlayout->addRow(i18n("Shape fill:"), colorlay);

        m_innerColor->setColor(m_lineAnn->lineInnerColor());
        if (m_lineAnn->lineInnerColor().isValid()) {
            m_useColor->setChecked(true);
        } else {
            m_innerColor->setEnabled(false);
        }

        addVerticalSpacer();
        formlayout->addRow(i18n("&Width:"), m_spinSize);

        connect(m_innerColor, &KColorButton::changed, this, &AnnotationWidget::dataChanged);
        connect(m_useColor, &QAbstractButton::toggled, this, &AnnotationWidget::dataChanged);
        connect(m_useColor, &QCheckBox::toggled, m_innerColor, &KColorButton::setEnabled);
    }
}

void LineAnnotationWidget::applyChanges()
{
    AnnotationWidget::applyChanges();
    if (m_lineType == 0) {
        Q_ASSERT(m_spinLL && m_spinLLE && m_startStyleCombo && m_endStyleCombo);
        m_lineAnn->setLineLeadingForwardPoint(m_spinLL->value());
        m_lineAnn->setLineLeadingBackwardPoint(m_spinLLE->value());
        m_lineAnn->setLineStartStyle((Okular::LineAnnotation::TermStyle)m_startStyleCombo->currentIndex());
        m_lineAnn->setLineEndStyle((Okular::LineAnnotation::TermStyle)m_endStyleCombo->currentIndex());
    } else if (m_lineType == 1) {
        Q_ASSERT(m_useColor && m_innerColor);
        if (!m_useColor->isChecked()) {
            m_lineAnn->setLineInnerColor(QColor());
        } else {
            m_lineAnn->setLineInnerColor(m_innerColor->color());
        }
    }
    Q_ASSERT(m_spinSize);
    m_lineAnn->style().setWidth(m_spinSize->value());
}

QIcon LineAnnotationWidget::endStyleIcon(Okular::LineAnnotation::TermStyle endStyle, const QColor &lineColor)
{
    const int iconSize {48};
    QImage image {iconSize, iconSize, QImage::Format_ARGB32};
    image.fill(qRgba(0, 0, 0, 0));
    Okular::LineAnnotation prototype;
    prototype.setLinePoints({{0, 0.5}, {0.65, 0.5}});
    prototype.setLineStartStyle(Okular::LineAnnotation::TermStyle::None);
    prototype.setLineEndStyle(endStyle);
    prototype.style().setWidth(4);
    prototype.style().setColor(lineColor);
    prototype.style().setLineStyle(Okular::Annotation::LineStyle::Solid);
    prototype.setBoundingRectangle({0, 0, 1, 1});
    LineAnnotPainter linepainter {&prototype, QSize {iconSize, iconSize}, 1, QTransform()};
    linepainter.draw(image);
    return QIcon(QPixmap::fromImage(image));
}

InkAnnotationWidget::InkAnnotationWidget(Okular::Annotation *ann)
    : AnnotationWidget(ann)
{
    m_inkAnn = static_cast<Okular::InkAnnotation *>(ann);
}

void InkAnnotationWidget::createStyleWidget(QFormLayout *formlayout)
{
    QWidget *widget = qobject_cast<QWidget *>(formlayout->parent());

    addColorButton(widget, formlayout);
    addOpacitySpinBox(widget, formlayout);

    addVerticalSpacer(formlayout);

    m_spinSize = new QDoubleSpinBox(widget);
    formlayout->addRow(i18n("&Width:"), m_spinSize);

    m_spinSize->setRange(1, 100);
    m_spinSize->setValue(m_inkAnn->style().width());

    connect(m_spinSize, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &AnnotationWidget::dataChanged);
}

void InkAnnotationWidget::applyChanges()
{
    AnnotationWidget::applyChanges();
    m_inkAnn->style().setWidth(m_spinSize->value());
}

HighlightAnnotationWidget::HighlightAnnotationWidget(Okular::Annotation *ann)
    : AnnotationWidget(ann)
{
    m_hlAnn = static_cast<Okular::HighlightAnnotation *>(ann);
}

void HighlightAnnotationWidget::createStyleWidget(QFormLayout *formlayout)
{
    QWidget *widget = qobject_cast<QWidget *>(formlayout->parent());

    m_typeCombo = new KComboBox(widget);
    m_typeCombo->setVisible(m_typeEditable);
    if (m_typeEditable) {
        formlayout->addRow(i18n("Type:"), m_typeCombo);
    }
    m_typeCombo->addItem(i18n("Highlight"));
    m_typeCombo->addItem(i18n("Squiggle"));
    m_typeCombo->addItem(i18n("Underline"));
    m_typeCombo->addItem(i18n("Strike out"));
    m_typeCombo->setCurrentIndex(m_hlAnn->highlightType());

    addVerticalSpacer(formlayout);
    addColorButton(widget, formlayout);
    addOpacitySpinBox(widget, formlayout);

    connect(m_typeCombo, QOverload<int>::of(&KComboBox::currentIndexChanged), this, &AnnotationWidget::dataChanged);
}

void HighlightAnnotationWidget::applyChanges()
{
    AnnotationWidget::applyChanges();
    m_hlAnn->setHighlightType((Okular::HighlightAnnotation::HighlightType)m_typeCombo->currentIndex());
}

GeomAnnotationWidget::GeomAnnotationWidget(Okular::Annotation *ann)
    : AnnotationWidget(ann)
{
    m_geomAnn = static_cast<Okular::GeomAnnotation *>(ann);
}

void GeomAnnotationWidget::createStyleWidget(QFormLayout *formlayout)
{
    QWidget *widget = qobject_cast<QWidget *>(formlayout->parent());

    m_typeCombo = new KComboBox(widget);
    m_typeCombo->setVisible(m_typeEditable);
    if (m_typeEditable) {
        formlayout->addRow(i18n("Type:"), m_typeCombo);
    }
    addVerticalSpacer(formlayout);
    addColorButton(widget, formlayout);
    addOpacitySpinBox(widget, formlayout);
    QHBoxLayout *colorlay = new QHBoxLayout();
    m_useColor = new QCheckBox(i18n("Enabled"), widget);
    colorlay->addWidget(m_useColor);
    m_innerColor = new KColorButton(widget);
    colorlay->addWidget(m_innerColor);
    formlayout->addRow(i18n("Shape fill:"), colorlay);
    addVerticalSpacer(formlayout);
    m_spinSize = new QDoubleSpinBox(widget);
    formlayout->addRow(i18n("&Width:"), m_spinSize);

    m_typeCombo->addItem(i18n("Rectangle"));
    m_typeCombo->addItem(i18n("Ellipse"));
    m_typeCombo->setCurrentIndex(m_geomAnn->geometricalType());
    m_innerColor->setColor(m_geomAnn->geometricalInnerColor());
    if (m_geomAnn->geometricalInnerColor().isValid()) {
        m_useColor->setChecked(true);
    } else {
        m_innerColor->setEnabled(false);
    }
    m_spinSize->setRange(0, 100);
    m_spinSize->setValue(m_geomAnn->style().width());

    connect(m_typeCombo, QOverload<int>::of(&KComboBox::currentIndexChanged), this, &AnnotationWidget::dataChanged);
    connect(m_innerColor, &KColorButton::changed, this, &AnnotationWidget::dataChanged);
    connect(m_useColor, &QAbstractButton::toggled, this, &AnnotationWidget::dataChanged);
    connect(m_useColor, &QCheckBox::toggled, m_innerColor, &KColorButton::setEnabled);
    connect(m_spinSize, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this, &AnnotationWidget::dataChanged);
}

void GeomAnnotationWidget::applyChanges()
{
    AnnotationWidget::applyChanges();
    m_geomAnn->setGeometricalType((Okular::GeomAnnotation::GeomType)m_typeCombo->currentIndex());
    if (!m_useColor->isChecked()) {
        m_geomAnn->setGeometricalInnerColor(QColor());
    } else {
        m_geomAnn->setGeometricalInnerColor(m_innerColor->color());
    }
    m_geomAnn->style().setWidth(m_spinSize->value());
}

FileAttachmentAnnotationWidget::FileAttachmentAnnotationWidget(Okular::Annotation *ann)
    : AnnotationWidget(ann)
    , m_pixmapSelector(nullptr)
{
    m_attachAnn = static_cast<Okular::FileAttachmentAnnotation *>(ann);
}

void FileAttachmentAnnotationWidget::createStyleWidget(QFormLayout *formlayout)
{
    QWidget *widget = qobject_cast<QWidget *>(formlayout->parent());

    addOpacitySpinBox(widget, formlayout);

    m_pixmapSelector = new PixmapPreviewSelector(widget);
    formlayout->addRow(i18n("File attachment symbol:"), m_pixmapSelector);
    m_pixmapSelector->setEditable(true);

    m_pixmapSelector->addItem(i18nc("Symbol for file attachment annotations", "Graph"), QStringLiteral("graph"));
    m_pixmapSelector->addItem(i18nc("Symbol for file attachment annotations", "Push Pin"), QStringLiteral("pushpin"));
    m_pixmapSelector->addItem(i18nc("Symbol for file attachment annotations", "Paperclip"), QStringLiteral("paperclip"));
    m_pixmapSelector->addItem(i18nc("Symbol for file attachment annotations", "Tag"), QStringLiteral("tag"));
    m_pixmapSelector->setIcon(m_attachAnn->fileIconName());

    connect(m_pixmapSelector, &PixmapPreviewSelector::iconChanged, this, &AnnotationWidget::dataChanged);
}

QWidget *FileAttachmentAnnotationWidget::createExtraWidget()
{
    QWidget *widget = new QWidget();
    widget->setWindowTitle(i18nc("'File' as normal file, that can be opened, saved, etc..", "File"));

    Okular::EmbeddedFile *ef = m_attachAnn->embeddedFile();
    const int size = ef->size();
    const QString sizeString = size <= 0 ? i18nc("Not available size", "N/A") : KFormat().formatByteSize(size);
    const QString descString = ef->description().isEmpty() ? i18n("No description available.") : ef->description();

    QHBoxLayout *mainLay = new QHBoxLayout(widget);
    QFormLayout *lay = new QFormLayout();
    mainLay->addLayout(lay);

    QLabel *tmplabel = new QLabel(ef->name(), widget);
    tmplabel->setTextInteractionFlags(Qt::TextSelectableByMouse);
    lay->addRow(i18n("Name:"), tmplabel);

    tmplabel = new QLabel(sizeString, widget);
    tmplabel->setTextInteractionFlags(Qt::TextSelectableByMouse);
    lay->addRow(i18n("&Width:"), tmplabel);

    tmplabel = new QLabel(widget);
    tmplabel->setTextFormat(Qt::PlainText);
    tmplabel->setWordWrap(true);
    tmplabel->setText(descString);
    tmplabel->setTextInteractionFlags(Qt::TextSelectableByMouse);
    lay->addRow(i18n("Description:"), tmplabel);

    QMimeDatabase db;
    QMimeType mime = db.mimeTypeForFile(ef->name(), QMimeDatabase::MatchExtension);
    if (mime.isValid()) {
        tmplabel = new QLabel(widget);
        tmplabel->setPixmap(QIcon::fromTheme(mime.iconName()).pixmap(FILEATTACH_ICONSIZE, FILEATTACH_ICONSIZE));
        tmplabel->setFixedSize(FILEATTACH_ICONSIZE, FILEATTACH_ICONSIZE);
        QVBoxLayout *tmpLayout = new QVBoxLayout(widget);
        tmpLayout->setAlignment(Qt::AlignTop);
        mainLay->addLayout(tmpLayout);
        tmpLayout->addWidget(tmplabel);
    }

    return widget;
}

void FileAttachmentAnnotationWidget::applyChanges()
{
    AnnotationWidget::applyChanges();
    m_attachAnn->setFileIconName(m_pixmapSelector->icon());
}

static QString caretSymbolToIcon(Okular::CaretAnnotation::CaretSymbol symbol)
{
    switch (symbol) {
    case Okular::CaretAnnotation::None:
        return QStringLiteral("caret-none");
    case Okular::CaretAnnotation::P:
        return QStringLiteral("caret-p");
    }
    return QString();
}

static Okular::CaretAnnotation::CaretSymbol caretSymbolFromIcon(const QString &icon)
{
    if (icon == QLatin1String("caret-none")) {
        return Okular::CaretAnnotation::None;
    } else if (icon == QLatin1String("caret-p")) {
        return Okular::CaretAnnotation::P;
    }
    return Okular::CaretAnnotation::None;
}

CaretAnnotationWidget::CaretAnnotationWidget(Okular::Annotation *ann)
    : AnnotationWidget(ann)
    , m_pixmapSelector(nullptr)
{
    m_caretAnn = static_cast<Okular::CaretAnnotation *>(ann);
}

void CaretAnnotationWidget::createStyleWidget(QFormLayout *formlayout)
{
    QWidget *widget = qobject_cast<QWidget *>(formlayout->parent());

    addColorButton(widget, formlayout);
    addOpacitySpinBox(widget, formlayout);

    m_pixmapSelector = new PixmapPreviewSelector(widget);
    formlayout->addRow(i18n("Caret symbol:"), m_pixmapSelector);

    m_pixmapSelector->addItem(i18nc("Symbol for caret annotations", "None"), QStringLiteral("caret-none"));
    m_pixmapSelector->addItem(i18nc("Symbol for caret annotations", "P"), QStringLiteral("caret-p"));
    m_pixmapSelector->setIcon(caretSymbolToIcon(m_caretAnn->caretSymbol()));

    connect(m_pixmapSelector, &PixmapPreviewSelector::iconChanged, this, &AnnotationWidget::dataChanged);
}

void CaretAnnotationWidget::applyChanges()
{
    AnnotationWidget::applyChanges();
    m_caretAnn->setCaretSymbol(caretSymbolFromIcon(m_pixmapSelector->icon()));
}
*/

#include "moc_annotationwidgets.cpp"
