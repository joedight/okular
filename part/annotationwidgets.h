/*
    SPDX-FileCopyrightText: 2006 Pino Toscano <pino@kde.org>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#ifndef _ANNOTATIONWIDGETS_H_
#define _ANNOTATIONWIDGETS_H_

#include <QWidget>
#include <QList>
#include <QSet>

#include "core/annotations.h"

class QCheckBox;
class QComboBox;
class QDoubleSpinBox;
class QFormLayout;
class QLabel;
class QPushButton;
class QWidget;
class KColorButton;
class QSpinBox;
class KFontRequester;
class AnnotationWidget;

class PixmapPreviewSelector : public QWidget
{
    Q_OBJECT

public:
    enum PreviewPosition { Side, Below };

    explicit PixmapPreviewSelector(QWidget *parent = nullptr, PreviewPosition position = Side);
    ~PixmapPreviewSelector() override;

    void setIcon(const QString &icon);
    QString icon() const;

    void addItem(const QString &item, const QString &id);

    void setPreviewSize(int size);
    int previewSize() const;

    void setEditable(bool editable);

Q_SIGNALS:
    void iconChanged(const QString &);

private Q_SLOTS:
    void iconComboChanged(const QString &icon);
    void selectCustomStamp();

private:
    QString m_icon;
    QPushButton *m_stampPushButton;
    QLabel *m_iconLabel;
    QComboBox *m_comboItems;
    int m_previewSize;
    PreviewPosition m_previewPosition;
};

/**
 * A factory to create AnnotationWidgets.
 */
class AnnotationWidgetFactory
{
public:
    static AnnotationWidget *appearanceWidgetFor(QWidget *parent, Okular::Annotation *ann);
    static AnnotationWidget *extraWidgetFor(QWidget *parent, Okular::Annotation *ann);
    static QList<AnnotationWidget*> appearanceWidgetsFor(QWidget *parent, const QList<Okular::Annotation*> &anns);
    static QList<AnnotationWidget*> extraWidgetsFor(QWidget *parent, const QList<Okular::Annotation*> &anns);
};

class AnnotationWidget : public QWidget
{
    Q_OBJECT

public:
    virtual void applyChanges();
    /* Return the annotations which would be modified by applyChanges() */
    virtual QSet<Okular::Annotation*> dirtyAnnotations();
    void setAnnotTypeEditable(bool);
    enum Flags {
        ExcludeSharedOptions = 0,
        IncludeSharedOptions = 1,
    };

    explicit AnnotationWidget(QWidget *parent, const QList<Okular::Annotation*> &anns, Flags flags);

Q_SIGNALS:
    void dataChanged();

protected:
    void addColorPicker();
    void addOpacitySpinBox();
    void addVerticalSpacer();

    bool m_typeEditable;

    QList<Okular::Annotation*> m_anns;
    QFormLayout *m_formlayout;

    KColorButton *m_colorBn {nullptr};
    QSpinBox *m_opacity {nullptr};
};

class TextAnnotationWidget : public AnnotationWidget
{
    Q_OBJECT

public:
    explicit TextAnnotationWidget(QWidget *parent, const QList<Okular::Annotation*> &anns, Flags flags);

    void applyChanges() override;

private:
    void createPopupNoteStyleUi();
    void createInlineNoteStyleUi();
    void createTypewriterStyleUi();
    void addPixmapSelector();
    void addFontRequester();
    void addTextColorButton();
    void addTextAlignComboBox();
    void addWidthSpinBox();

    QList<Okular::TextAnnotation*> m_textLinked, m_textTypewriter, m_textInplace;
    PixmapPreviewSelector *m_pixmapSelector {nullptr};
    KFontRequester *m_fontReq {nullptr};
    KColorButton *m_textColorBn {nullptr};
    QComboBox *m_textAlign {nullptr};
    QDoubleSpinBox *m_spinWidth {nullptr};
};

class StampAnnotationWidget : public AnnotationWidget
{
    Q_OBJECT

public:
    static const QList<QPair<QString, QString>> &defaultStamps();

    explicit StampAnnotationWidget(QWidget *parent, const QList<Okular::Annotation*> &anns, Flags flags);

    void applyChanges() override;

private:
    Okular::StampAnnotation *m_stampAnn;
    PixmapPreviewSelector *m_pixmapSelector;
};

/*
class GeneralLineAnnotationWidget : public AnnotationWidget
{
    Q_OBJECT

public:
    explicit LineAnnotationWidget(QObject *parent, Okular::Annotation *ann);

    void applyChanges() override;

protected:
    bool addWidthSpinbox();

private:
    static QIcon endStyleIcon(Okular::LineAnnotation::TermStyle endStyle, const QColor &lineColor);

    QDoubleSpinBox *m_spinWidth {nullptr};

    QDoubleSpinBox *m_spinLL {nullptr};
    QDoubleSpinBox *m_spinLLE {nullptr};
    QCheckBox *m_useColor {nullptr};
    KColorButton *m_innerColor {nullptr};
    QComboBox *m_startStyleCombo {nullptr};
    QComboBox *m_endStyleCombo {nullptr};
};

class TwopointLineAnnotationWidget : public GenericLineAnnotationWidget
{

}

class PolygonLineAnnotationWidget : public GenericLineAnnotationWidget
{

}


class HighlightAnnotationWidget : public AnnotationWidget
{
    Q_OBJECT

public:
    explicit HighlightAnnotationWidget(Okular::Annotation *ann);

    void applyChanges() override;

protected:
    void createStyleWidget(QFormLayout *formlayout) override;

private:
    Okular::HighlightAnnotation *m_hlAnn;
    QComboBox *m_typeCombo = nullptr;
};

class GeomAnnotationWidget : public AnnotationWidget
{
    Q_OBJECT

public:
    explicit GeomAnnotationWidget(Okular::Annotation *ann);

    void applyChanges() override;

protected:
    void createStyleWidget(QFormLayout *formlayout) override;

private:
    Okular::GeomAnnotation *m_geomAnn;
    QComboBox *m_typeCombo = nullptr;
    QCheckBox *m_useColor = nullptr;
    KColorButton *m_innerColor = nullptr;
    QDoubleSpinBox *m_spinSize = nullptr;
};

class FileAttachmentAnnotationWidget : public AnnotationWidget
{
    Q_OBJECT

public:
    explicit FileAttachmentAnnotationWidget(Okular::Annotation *ann);

    void applyChanges() override;

protected:
    void createStyleWidget(QFormLayout *formlayout) override;
    QWidget *createExtraWidget() override;

private:
    Okular::FileAttachmentAnnotation *m_attachAnn;
    PixmapPreviewSelector *m_pixmapSelector;
};

class CaretAnnotationWidget : public AnnotationWidget
{
    Q_OBJECT

public:
    explicit CaretAnnotationWidget(Okular::Annotation *ann);

    void applyChanges() override;

protected:
    void createStyleWidget(QFormLayout *formlayout) override;

private:
    Okular::CaretAnnotation *m_caretAnn;
    PixmapPreviewSelector *m_pixmapSelector;
};

class InkAnnotationWidget : public AnnotationWidget
{
    Q_OBJECT

public:
    explicit InkAnnotationWidget(Okular::Annotation *ann);

    void applyChanges() override;

protected:
    void createStyleWidget(QFormLayout *formlayout) override;

private:
    Okular::InkAnnotation *m_inkAnn;
    QDoubleSpinBox *m_spinSize = nullptr;
};
*/

#endif
