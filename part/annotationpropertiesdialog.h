/*
    SPDX-FileCopyrightText: 2006 Chu Xiaodong <xiaodongchu@gmail.com>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#ifndef _ANNOTATIONPROPERTIESDIALOG_H_
#define _ANNOTATIONPROPERTIESDIALOG_H_

#include <KPageDialog>

class QLabel;
class QLineEdit;
class AnnotationWidget;

namespace Okular
{
class AnnotPagePair;
class Annotation;
class Document;
}

class AnnotsPropertiesDialog : public KPageDialog
{
    Q_OBJECT
public:
    AnnotsPropertiesDialog(QWidget *parent, Okular::Document *document, const QList<Okular::AnnotPagePair> &anns);
    ~AnnotsPropertiesDialog() override;

private:
    Okular::Document *m_document;
    int m_page;
    bool modified;
    QList<Okular::AnnotPagePair> m_annots;
    // dialog widgets:
    QLineEdit *AuthorEdit;
    QList<AnnotationWidget*> m_appearanceWidgets, m_extraWidgets;
    QLabel *m_modifyDateLabel;

    void setCaptionTextbyAnnotType();

private Q_SLOTS:
    void setModified();
    void slotapply();
};

#endif
